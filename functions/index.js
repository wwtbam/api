const admin = require('firebase-admin');
const functions = require('firebase-functions');

admin.initializeApp();

const db = admin.firestore();
db.settings({ timestampsInSnapshots: true});

function getRandomQuestionByLevel(level, questions) {
  let filteredQuestions = questions.filter(question => { return question.difficulty === level });
  let randomQuestion = filteredQuestions[Math.floor(Math.random() * filteredQuestions.length)];
  randomQuestion.answers.map(answer => answer.selectable = true);
  return randomQuestion;
}

function createLevel(level, questions) {
  let question = getRandomQuestionByLevel(level, questions);

  switch (level) {
    case 1: return {difficulty: level, prize: 800, safe: false, completed: false, question: question};
    case 2: return {difficulty: level, prize: 1500, safe: true, completed: false, question: question};
    case 3: return {difficulty: level, prize: 3000, safe: false, completed: false, question: question};
    case 4: return {difficulty: level, prize: 6000, safe: false, completed: false, question: question};
    case 5: return {difficulty: level, prize: 12000, safe: false, completed: false, question: question};
    case 6: return {difficulty: level, prize: 24000, safe: false, completed: false, question: question};
    case 7: return {difficulty: level, prize: 48000, safe: true, completed: false, question: question};
    case 8: return {difficulty: level, prize: 72000, safe: false, completed: false, question: question};
    case 9: return {difficulty: level, prize: 100000, safe: false, completed: false, question: question};
    case 10: return {difficulty: level, prize: 150000, safe: false, completed: false, question: question};
    case 11: return {difficulty: level, prize: 300000, safe: false, completed: false, question: question};
    case 12: return {difficulty: level, prize: 1000000, safe: true, completed: false, question: question};
    default: return {};
  }
}

function initLevels(questions) {
  let levels = [];

  for (let i = 0; i < 12; i++) {
    const level = createLevel(i + 1, questions);
    levels.push(level)
  }

  return levels;
}

startGame = (data, context) => {
  if (!context.auth) {
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called while authenticated.');
  }

  return db.collection('questions').get().then(snapshot => {
      let questions = [];
      snapshot.forEach(doc => questions.push(doc.data()));

      let playerId = context.auth.uid;
      let jokers = [{ type: "FIFTY_FIFTY", used: false }, { type: "CALL_A_FRIEND", used: false }, { type: "AUDIENCE_VOTE", used: false }, { type: "SWITCH", used: false }];
      let levels = initLevels(questions);
      let game = { playerId: playerId, terminated: false, jokers: jokers, levels: levels};

      return db.collection("games").add(game)
        .then(reference => {
          return db.collection("users").doc(playerId).update({ "game": reference })
        });
    })
    .catch(error => { return error });
};

endGame = (data, context) => {
  if (!context.auth) {
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called while authenticated.');
  }

  const userId = context.auth.uid;
  const userReference = db.collection('users').doc(userId);

  return userReference.get()
    .then(userDoc => {
      if (!userDoc.exists) {
        throw new functions.https.HttpsError('failed-precondition', 'Player does not exist');
      }

      const user = userDoc.data();
      const gameReference = user.game;

      return gameReference.get()
        .then(gameDoc => {
          if (!gameDoc.exists) {
            throw new functions.https.HttpsError('failed-precondition', 'Player has no game currently running');
          }

          const game = gameDoc.data();
          const level = game.levels.reverse().find(level => level.completed);
          const levelSafe = game.levels.find(level => level.completed && level.safe);
          const jokersUsedCount = game.jokers.filter(joker => joker.used).length;
          const score = {
            game: gameReference,
            user: userReference,
            userName: user.name,
            levelReached: level ? level.difficulty : 0,
            prizeEarned: levelSafe ? levelSafe.prize : 0,
            jokersUsedCount: jokersUsedCount,
            creationDate: Date.now()
          };

          return db.collection('users').doc(userId).collection('scores').add(score)
            .then(() => { return userReference.update({ game: null }) })
        })
    })
};

createUser = (user) => {
  const userName = user.email.split("@")[0];
  const data = { email: user.email, name: userName, game: null };
  return db.collection('users').doc(user.uid).set(data);
};

deleteUser = (user) => {
  return db.collection('users').doc(user.uid).delete();
};

useJoker = (data, context) => {
  if (!context.auth) {
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called while authenticated.');
  }

  const userId = context.auth.uid;
  const jokerType = data.jokerType;

  return db.collection('users').doc(userId).get()
    .then(userDoc => {
      if (!userDoc.exists) {
        throw new functions.https.HttpsError('failed-precondition', 'Player does not exist');
      }

      const user = userDoc.data();
      const gameReference = user.game;

      return gameReference.get()
        .then(gameDoc => {
          if (!gameDoc.exists) {
            throw new functions.https.HttpsError('failed-precondition', 'Player has no game currently running');
          }

          const game = gameDoc.data();
          const currentLevel = game.levels.find(level => !level.completed);
          let total = 100.0;
          let randomAnswersFifty = game.levels.find(level => !level.completed).question.answers
            .filter(answer => { return !answer.correct && answer.selectable});
          randomAnswersFifty = randomAnswersFifty.sort(() => { return 0.5 - Math.random() });

          let answerFriendSelectionMax = 100 - (5 * currentLevel.question.difficulty);
          let answerFriendSelectionMin = answerFriendSelectionMax - 10;
          let answerFriendSelection = parseFloat((Math.random() * (answerFriendSelectionMax - answerFriendSelectionMin) + answerFriendSelectionMin).toFixed(2));
          let answerFriendRandom = Math.random() * 100;
          let audienceCorrectMax = 100 - (6 * currentLevel.question.difficulty);
          let audienceCorrectMin = audienceCorrectMax - 10;
          let audienceCorrectRand = parseFloat((Math.random() * (audienceCorrectMax - audienceCorrectMin) + audienceCorrectMin).toFixed(2));

          switch(jokerType) {
            case "FIFTY_FIFTY":
              game.levels.find(level => !level.completed).question.answers.find(answer => answer.statement === randomAnswersFifty[0].statement).selectable = false;
              game.levels.find(level => !level.completed).question.answers.find(answer => answer.statement === randomAnswersFifty[1].statement).selectable = false;
              break;
            case "CALL_A_FRIEND":
              if (answerFriendRandom <= answerFriendSelection) {
                game.levels.find(level => !level.completed).question.answers.find(answer => answer.correct).friendSelection = true;
              } else {
                game.levels.find(level => !level.completed).question.answers[Math.floor(Math.random() * 4)].friendSelection = true;
              }
              break;
            case "AUDIENCE_VOTE":
              game.levels.find(level => !level.completed).question.answers.find(answer => answer.correct).score = audienceCorrectRand;
              total -= audienceCorrectRand;

              game.levels.find(level => !level.completed).question.answers.forEach((answer, index, array) => {
                if (answer.selectable && !answer.correct) {
                  let score = parseFloat((Math.random() * total).toFixed(2));
                  answer.score = index === array.length - 1 ? total : score;
                  total = parseFloat((total - answer.score).toFixed(2));
                }
              });
              break;
            case "SWITCH":
              return db.collection('questions')
                .where("difficulty", "==", currentLevel.question.difficulty)
                .get()
                .then(snapshot => {
                  let documents = [];
                  snapshot.forEach(doc => documents.push(doc.data()));

                  if (documents.length === 0) {
                    throw new functions.https.HttpsError('failed-precondition', 'No new question matching request');
                  }

                  game.levels.find(level => !level.completed).question = documents[Math.floor(Math.random() * documents.length)];
                  game.jokers.find(joker => joker.type === jokerType).used = true;
                  return gameReference.set(game)
                });
          }

          game.jokers.find(joker => joker.type === jokerType).used = true;
          return gameReference.set(game)
        });
    });
};

confirmAnswer = (data, context) => {
  if (!context.auth) {
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called while authenticated.');
  }

  const userId = context.auth.uid;
  const answerIndex = data.answerIndex;

  return db.collection('users').doc(userId).get()
    .then(userDoc => {
      if (!userDoc.exists) {
        throw new functions.https.HttpsError('failed-precondition', 'Player does not exist');
      }

      const user = userDoc.data();
      const gameReference = user.game;

      return gameReference.get()
        .then(gameDoc => {
          if (!gameDoc.exists) {
            throw new functions.https.HttpsError('failed-precondition', 'Player has no game currently running');
          }

          const game = gameDoc.data();
          const currentLevel = game.levels.find(level => !level.completed);

          if (currentLevel) {
            const hasCorrectAnswer = currentLevel.question.answers[answerIndex].correct || false;
            currentLevel.completed = hasCorrectAnswer;

            if (!hasCorrectAnswer) {
              game.terminated = true
            }
          }

          if (!game.terminated) {
            game.terminated = game.levels.filter(level => !level.completed).length === 0;
          }

          if (game.terminated) {
            return gameReference.set(game).then(() => { return endGame(data, context)});
          } else {
            return gameReference.set(game)
          }
        });
    });
};

exports.startGame = functions.https.onCall(startGame);
exports.endGame = functions.https.onCall(endGame);
exports.useJoker = functions.https.onCall(useJoker);
exports.confirmAnswer = functions.https.onCall(confirmAnswer);
exports.createUser = functions.auth.user().onCreate(createUser);
exports.deleteUser = functions.auth.user().onDelete(deleteUser);
